-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Host: mysql669.umbler.com
-- Generation Time: 09-Jun-2020 às 00:17
-- Versão do servidor: 5.6.40
-- PHP Version: 5.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ragna-memory-cms`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) unsigned NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Description` longtext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cities`
--

INSERT INTO `cities` (`id`, `Name`, `Description`, `created_at`, `updated_at`) VALUES
(1, 'Al de Baran', NULL, '2020-06-09 05:27:58', '2020-06-09 05:27:58'),
(2, 'Alberta', NULL, '2020-06-09 05:28:08', '2020-06-09 05:28:08'),
(3, 'Amatsu', NULL, '2020-06-09 05:28:14', '2020-06-09 05:28:14'),
(4, 'Ayothaya', NULL, '2020-06-09 05:28:19', '2020-06-09 05:28:19'),
(5, 'Brasilis', NULL, '2020-06-09 05:28:24', '2020-06-09 05:28:24'),
(6, 'Comodo', NULL, '2020-06-09 05:28:28', '2020-06-09 05:28:28'),
(7, 'Dewata', NULL, '2020-06-09 05:28:33', '2020-06-09 05:28:33'),
(8, 'Einbroch', NULL, '2020-06-09 05:28:38', '2020-06-09 05:28:38'),
(9, 'Geffen', NULL, '2020-06-09 05:28:42', '2020-06-09 05:28:42'),
(10, 'Glast Heim', NULL, '2020-06-09 05:28:47', '2020-06-09 05:28:47'),
(11, 'Hugel', NULL, '2020-06-09 05:28:57', '2020-06-09 05:28:57'),
(12, 'Ilha sem Nome', NULL, '2020-06-09 05:29:02', '2020-06-09 05:29:02'),
(13, 'Izlude', NULL, '2020-06-09 05:29:06', '2020-06-09 05:29:06'),
(14, 'Jawaii', NULL, '2020-06-09 05:29:10', '2020-06-09 05:29:10'),
(15, 'Juno', NULL, '2020-06-09 05:29:14', '2020-06-09 05:29:14'),
(16, 'Kunlun', NULL, '2020-06-09 05:29:18', '2020-06-09 05:29:18'),
(17, 'Lighthalzen', NULL, '2020-06-09 05:29:22', '2020-06-09 05:29:22'),
(18, 'Louyang', NULL, '2020-06-09 05:29:26', '2020-06-09 05:29:26'),
(19, 'Lutie', NULL, '2020-06-09 05:29:30', '2020-06-09 05:29:30'),
(20, 'Morroc', NULL, '2020-06-09 05:29:35', '2020-06-09 05:29:35'),
(21, 'Moscóvia', NULL, '2020-06-09 05:29:42', '2020-06-09 05:29:42'),
(22, 'Mosteiro St. Capitolina', NULL, '2020-06-09 05:29:47', '2020-06-09 05:29:47'),
(23, 'Nação de Arunafeltz', NULL, '2020-06-09 05:29:53', '2020-06-09 05:29:53'),
(24, 'Nifflheim', NULL, '2020-06-09 05:29:57', '2020-06-09 05:29:57'),
(25, 'Payon', NULL, '2020-06-09 05:30:02', '2020-06-09 05:30:02'),
(26, 'Prontera', NULL, '2020-06-09 05:30:06', '2020-06-09 05:30:06'),
(27, 'Rachel', NULL, '2020-06-09 05:30:11', '2020-06-09 05:30:11'),
(28, 'Umbala', NULL, '2020-06-09 05:30:15', '2020-06-09 05:30:15'),
(29, 'Veins', NULL, '2020-06-09 05:30:20', '2020-06-09 05:30:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `core_store`
--

CREATE TABLE IF NOT EXISTS `core_store` (
  `id` int(10) unsigned NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext,
  `type` varchar(255) DEFAULT NULL,
  `environment` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `core_store`
--

INSERT INTO `core_store` (`id`, `key`, `value`, `type`, `environment`, `tag`) VALUES
(1, 'db_model_pages', '{"title":{"type":"string"},"content":{"type":"richtext"},"reference":{"type":"string"},"logo":{"model":"file","via":"related","plugin":"upload","required":false},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}', 'object', NULL, NULL),
(2, 'db_model_strapi_webhooks', '{"name":{"type":"string"},"url":{"type":"text"},"headers":{"type":"json"},"events":{"type":"json"},"enabled":{"type":"boolean"}}', 'object', NULL, NULL),
(3, 'db_model_upload_file', '{"name":{"type":"string","configurable":false,"required":true},"alternativeText":{"type":"string","configurable":false},"caption":{"type":"string","configurable":false},"width":{"type":"integer","configurable":false},"height":{"type":"integer","configurable":false},"formats":{"type":"json","configurable":false},"hash":{"type":"string","configurable":false,"required":true},"ext":{"type":"string","configurable":false},"mime":{"type":"string","configurable":false,"required":true},"size":{"type":"decimal","configurable":false,"required":true},"url":{"type":"string","configurable":false,"required":true},"previewUrl":{"type":"string","configurable":false},"provider":{"type":"string","configurable":false,"required":true},"provider_metadata":{"type":"json","configurable":false},"related":{"collection":"*","filter":"field","configurable":false},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}', 'object', NULL, NULL),
(4, 'db_model_core_store', '{"key":{"type":"string"},"value":{"type":"text"},"type":{"type":"string"},"environment":{"type":"string"},"tag":{"type":"string"}}', 'object', NULL, NULL),
(5, 'db_model_users-permissions_permission', '{"type":{"type":"string","required":true,"configurable":false},"controller":{"type":"string","required":true,"configurable":false},"action":{"type":"string","required":true,"configurable":false},"enabled":{"type":"boolean","required":true,"configurable":false},"policy":{"type":"string","configurable":false},"role":{"model":"role","via":"permissions","plugin":"users-permissions","configurable":false}}', 'object', NULL, NULL),
(6, 'db_model_users-permissions_user', '{"username":{"type":"string","minLength":3,"unique":true,"configurable":false,"required":true},"email":{"type":"email","minLength":6,"configurable":false,"required":true},"provider":{"type":"string","configurable":false},"password":{"type":"password","minLength":6,"configurable":false,"private":true},"resetPasswordToken":{"type":"string","configurable":false,"private":true},"confirmed":{"type":"boolean","default":false,"configurable":false},"blocked":{"type":"boolean","default":false,"configurable":false},"role":{"model":"role","via":"users","plugin":"users-permissions","configurable":false},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}', 'object', NULL, NULL),
(7, 'db_model_strapi_administrator', '{"username":{"type":"string","minLength":3,"unique":true,"configurable":false,"required":true},"email":{"type":"email","minLength":6,"configurable":false,"required":true},"password":{"type":"password","minLength":6,"configurable":false,"private":true,"required":true},"resetPasswordToken":{"type":"string","configurable":false,"private":true},"blocked":{"type":"boolean","default":false,"configurable":false}}', 'object', NULL, NULL),
(8, 'db_model_users-permissions_role', '{"name":{"type":"string","minLength":3,"required":true,"configurable":false},"description":{"type":"string","configurable":false},"type":{"type":"string","unique":true,"configurable":false},"permissions":{"collection":"permission","via":"role","plugin":"users-permissions","configurable":false,"isVirtual":true},"users":{"collection":"user","via":"role","configurable":false,"plugin":"users-permissions","isVirtual":true}}', 'object', NULL, NULL),
(9, 'db_model_upload_file_morph', '{"upload_file_id":{"type":"integer"},"related_id":{"type":"integer"},"related_type":{"type":"text"},"field":{"type":"text"},"order":{"type":"integer"}}', 'object', NULL, NULL),
(10, 'plugin_email_provider', '{"provider":"sendmail","name":"Sendmail","auth":{"sendmail_default_from":{"label":"Sendmail Default From","type":"text"},"sendmail_default_replyto":{"label":"Sendmail Default Reply-To","type":"text"}}}', 'object', 'production', ''),
(11, 'plugin_upload_settings', '{"sizeOptimization":true,"responsiveDimensions":true}', 'object', 'production', ''),
(12, 'plugin_content_manager_configuration_content_types::application::page.page', '{"uid":"application::page.page","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"title","defaultSortBy":"title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"content":{"edit":{"label":"Content","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Content","searchable":false,"sortable":false}},"reference":{"edit":{"label":"Reference","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Reference","searchable":true,"sortable":true}},"logo":{"edit":{"label":"Logo","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Logo","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","title","reference","logo"],"editRelations":[],"edit":[[{"name":"title","size":6}],[{"name":"content","size":12}],[{"name":"reference","size":6},{"name":"logo","size":6}]]}}', 'object', '', ''),
(13, 'plugin_users-permissions_grant', '{"email":{"enabled":true,"icon":"envelope"},"discord":{"enabled":false,"icon":"discord","key":"","secret":"","callback":"/auth/discord/callback","scope":["identify","email"]},"facebook":{"enabled":false,"icon":"facebook-square","key":"","secret":"","callback":"/auth/facebook/callback","scope":["email"]},"google":{"enabled":false,"icon":"google","key":"","secret":"","callback":"/auth/google/callback","scope":["email"]},"github":{"enabled":false,"icon":"github","key":"","secret":"","redirect_uri":"/auth/github/callback","scope":["user","user:email"]},"microsoft":{"enabled":false,"icon":"windows","key":"","secret":"","callback":"/auth/microsoft/callback","scope":["user.read"]},"twitter":{"enabled":false,"icon":"twitter","key":"","secret":"","callback":"/auth/twitter/callback"},"instagram":{"enabled":false,"icon":"instagram","key":"","secret":"","callback":"/auth/instagram/callback"},"vk":{"enabled":false,"icon":"vk","key":"","secret":"","callback":"/auth/vk/callback","scope":["email"]}}', 'object', '', ''),
(14, 'plugin_content_manager_configuration_content_types::plugins::users-permissions.permission', '{"uid":"plugins::users-permissions.permission","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"type","defaultSortBy":"type","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"controller":{"edit":{"label":"Controller","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Controller","searchable":true,"sortable":true}},"action":{"edit":{"label":"Action","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Action","searchable":true,"sortable":true}},"enabled":{"edit":{"label":"Enabled","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Enabled","searchable":true,"sortable":true}},"policy":{"edit":{"label":"Policy","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Policy","searchable":true,"sortable":true}},"role":{"edit":{"label":"Role","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Role","searchable":false,"sortable":false}}},"layouts":{"list":["id","type","controller","action"],"editRelations":["role"],"edit":[[{"name":"type","size":6},{"name":"controller","size":6}],[{"name":"action","size":6},{"name":"enabled","size":4}],[{"name":"policy","size":6}]]}}', 'object', '', ''),
(15, 'plugin_content_manager_configuration_content_types::plugins::upload.file', '{"uid":"plugins::upload.file","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"alternativeText":{"edit":{"label":"AlternativeText","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"AlternativeText","searchable":true,"sortable":true}},"caption":{"edit":{"label":"Caption","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Caption","searchable":true,"sortable":true}},"width":{"edit":{"label":"Width","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Width","searchable":true,"sortable":true}},"height":{"edit":{"label":"Height","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Height","searchable":true,"sortable":true}},"formats":{"edit":{"label":"Formats","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Formats","searchable":false,"sortable":false}},"hash":{"edit":{"label":"Hash","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Hash","searchable":true,"sortable":true}},"ext":{"edit":{"label":"Ext","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Ext","searchable":true,"sortable":true}},"mime":{"edit":{"label":"Mime","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Mime","searchable":true,"sortable":true}},"size":{"edit":{"label":"Size","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Size","searchable":true,"sortable":true}},"url":{"edit":{"label":"Url","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Url","searchable":true,"sortable":true}},"previewUrl":{"edit":{"label":"PreviewUrl","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"PreviewUrl","searchable":true,"sortable":true}},"provider":{"edit":{"label":"Provider","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Provider","searchable":true,"sortable":true}},"provider_metadata":{"edit":{"label":"Provider_metadata","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Provider_metadata","searchable":false,"sortable":false}},"related":{"edit":{"label":"Related","description":"","placeholder":"","visible":true,"editable":true,"mainField":"id"},"list":{"label":"Related","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","name","alternativeText","caption"],"editRelations":["related"],"edit":[[{"name":"name","size":6},{"name":"alternativeText","size":6}],[{"name":"caption","size":6},{"name":"width","size":4}],[{"name":"height","size":4}],[{"name":"formats","size":12}],[{"name":"hash","size":6},{"name":"ext","size":6}],[{"name":"mime","size":6},{"name":"size","size":4}],[{"name":"url","size":6},{"name":"previewUrl","size":6}],[{"name":"provider","size":6}],[{"name":"provider_metadata","size":12}]]}}', 'object', '', ''),
(16, 'plugin_content_manager_configuration_content_types::strapi::administrator', '{"uid":"strapi::administrator","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"username","defaultSortBy":"username","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"username":{"edit":{"label":"Username","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Username","searchable":true,"sortable":true}},"email":{"edit":{"label":"Email","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Email","searchable":true,"sortable":true}},"password":{"edit":{"label":"Password","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Password","searchable":true,"sortable":true}},"resetPasswordToken":{"edit":{"label":"ResetPasswordToken","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"ResetPasswordToken","searchable":true,"sortable":true}},"blocked":{"edit":{"label":"Blocked","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Blocked","searchable":true,"sortable":true}}},"layouts":{"list":["id","username","email","blocked"],"editRelations":[],"edit":[[{"name":"username","size":6},{"name":"email","size":6}],[{"name":"password","size":6},{"name":"blocked","size":4}]]}}', 'object', '', ''),
(17, 'plugin_content_manager_configuration_content_types::plugins::users-permissions.role', '{"uid":"plugins::users-permissions.role","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"name","defaultSortBy":"name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":true,"sortable":true}},"type":{"edit":{"label":"Type","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Type","searchable":true,"sortable":true}},"permissions":{"edit":{"label":"Permissions","description":"","placeholder":"","visible":true,"editable":true,"mainField":"type"},"list":{"label":"Permissions","searchable":false,"sortable":false}},"users":{"edit":{"label":"Users","description":"","placeholder":"","visible":true,"editable":true,"mainField":"username"},"list":{"label":"Users","searchable":false,"sortable":false}}},"layouts":{"list":["id","name","description","type"],"editRelations":["permissions","users"],"edit":[[{"name":"name","size":6},{"name":"description","size":6}],[{"name":"type","size":6}]]}}', 'object', '', ''),
(18, 'plugin_content_manager_configuration_content_types::plugins::users-permissions.user', '{"uid":"plugins::users-permissions.user","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"username","defaultSortBy":"username","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"username":{"edit":{"label":"Username","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Username","searchable":true,"sortable":true}},"email":{"edit":{"label":"Email","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Email","searchable":true,"sortable":true}},"provider":{"edit":{"label":"Provider","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Provider","searchable":true,"sortable":true}},"password":{"edit":{"label":"Password","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Password","searchable":true,"sortable":true}},"resetPasswordToken":{"edit":{"label":"ResetPasswordToken","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"ResetPasswordToken","searchable":true,"sortable":true}},"confirmed":{"edit":{"label":"Confirmed","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Confirmed","searchable":true,"sortable":true}},"blocked":{"edit":{"label":"Blocked","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Blocked","searchable":true,"sortable":true}},"role":{"edit":{"label":"Role","description":"","placeholder":"","visible":true,"editable":true,"mainField":"name"},"list":{"label":"Role","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","username","email","confirmed"],"editRelations":["role"],"edit":[[{"name":"username","size":6},{"name":"email","size":6}],[{"name":"password","size":6},{"name":"confirmed","size":4}],[{"name":"blocked","size":4}]]}}', 'object', '', ''),
(19, 'plugin_users-permissions_email', '{"reset_password":{"display":"Email.template.reset_password","icon":"sync","options":{"from":{"name":"Administration Panel","email":"no-reply@strapi.io"},"response_email":"","object":"Reset password","message":"<p>We heard that you lost your password. Sorry about that!</p>\\n\\n<p>But don’t worry! You can use the following link to reset your password:</p>\\n\\n<p><%= URL %>?code=<%= TOKEN %></p>\\n\\n<p>Thanks.</p>"}},"email_confirmation":{"display":"Email.template.email_confirmation","icon":"check-square","options":{"from":{"name":"Administration Panel","email":"no-reply@strapi.io"},"response_email":"","object":"Account confirmation","message":"<p>Thank you for registering!</p>\\n\\n<p>You have to confirm your email address. Please click on the link below.</p>\\n\\n<p><%= URL %>?confirmation=<%= CODE %></p>\\n\\n<p>Thanks.</p>"}}}', 'object', '', ''),
(20, 'plugin_users-permissions_advanced', '{"unique_email":true,"allow_register":true,"email_confirmation":false,"email_confirmation_redirection":"http://0.0.0.0:3000/admin","email_reset_password":"http://0.0.0.0:3000/admin","default_role":"authenticated"}', 'object', '', ''),
(21, 'db_model_current_episodes', '{"episode":{"model":"episode"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}', 'object', NULL, NULL),
(22, 'db_model_cities', '{"Name":{"type":"string","required":true,"unique":true},"Description":{"type":"richtext"},"picture":{"model":"file","via":"related","allowedTypes":["files","images","videos"],"plugin":"upload","required":false},"quests":{"via":"city","collection":"quest","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}', 'object', NULL, NULL),
(23, 'db_model_quests', '{"Title":{"type":"string","required":true,"unique":true},"Recipe":{"type":"richtext","required":true},"city":{"model":"city","via":"quests"},"episode":{"model":"episode","via":"quests"},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}', 'object', NULL, NULL),
(24, 'db_model_episodes', '{"Title":{"type":"string","required":true,"unique":true},"Order":{"type":"integer","required":true,"unique":true},"quests":{"via":"episode","collection":"quest","isVirtual":true},"created_at":{"type":"currentTimestamp"},"updated_at":{"type":"currentTimestamp"}}', 'object', NULL, NULL),
(25, 'plugin_content_manager_configuration_content_types::application::city.city', '{"uid":"application::city.city","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":50,"mainField":"Name","defaultSortBy":"Name","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"Name":{"edit":{"label":"Name","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Name","searchable":true,"sortable":true}},"Description":{"edit":{"label":"Description","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Description","searchable":false,"sortable":false}},"picture":{"edit":{"label":"Picture","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Picture","searchable":false,"sortable":false}},"quests":{"edit":{"label":"Quests","description":"","placeholder":"","visible":true,"editable":true,"mainField":"Title"},"list":{"label":"Quests","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["Name","picture"],"editRelations":["quests"],"edit":[[{"name":"Name","size":6}],[{"name":"Description","size":12}],[{"name":"picture","size":6}]]}}', 'object', '', ''),
(26, 'plugin_content_manager_configuration_content_types::application::current-episode.current-episode', '{"uid":"application::current-episode.current-episode","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":10,"mainField":"id","defaultSortBy":"id","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"episode":{"edit":{"label":"Episode","description":"","placeholder":"","visible":true,"editable":true,"mainField":"Title"},"list":{"label":"Episode","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["id","created_at","updated_at"],"editRelations":["episode"],"edit":[]}}', 'object', '', ''),
(27, 'plugin_content_manager_configuration_content_types::application::episode.episode', '{"uid":"application::episode.episode","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":50,"mainField":"Title","defaultSortBy":"Order","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"Title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"Order":{"edit":{"label":"Order","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Order","searchable":true,"sortable":true}},"quests":{"edit":{"label":"Quests","description":"","placeholder":"","visible":true,"editable":true,"mainField":"Title"},"list":{"label":"Quests","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["Order","Title"],"editRelations":["quests"],"edit":[[{"name":"Title","size":6},{"name":"Order","size":4}]]}}', 'object', '', ''),
(28, 'plugin_content_manager_configuration_content_types::application::quest.quest', '{"uid":"application::quest.quest","settings":{"bulkable":true,"filterable":true,"searchable":true,"pageSize":50,"mainField":"Title","defaultSortBy":"Title","defaultSortOrder":"ASC"},"metadatas":{"id":{"edit":{},"list":{"label":"Id","searchable":true,"sortable":true}},"Title":{"edit":{"label":"Title","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Title","searchable":true,"sortable":true}},"Recipe":{"edit":{"label":"Recipe","description":"","placeholder":"","visible":true,"editable":true},"list":{"label":"Recipe","searchable":false,"sortable":false}},"city":{"edit":{"label":"City","description":"","placeholder":"","visible":true,"editable":true,"mainField":"Name"},"list":{"label":"City","searchable":false,"sortable":false}},"episode":{"edit":{"label":"Episode","description":"","placeholder":"","visible":true,"editable":true,"mainField":"Title"},"list":{"label":"Episode","searchable":false,"sortable":false}},"created_at":{"edit":{"label":"Created_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Created_at","searchable":true,"sortable":true}},"updated_at":{"edit":{"label":"Updated_at","description":"","placeholder":"","visible":false,"editable":true},"list":{"label":"Updated_at","searchable":true,"sortable":true}}},"layouts":{"list":["Title"],"editRelations":["city","episode"],"edit":[[{"name":"Title","size":6}],[{"name":"Recipe","size":12}]]}}', 'object', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `current_episodes`
--

CREATE TABLE IF NOT EXISTS `current_episodes` (
  `id` int(10) unsigned NOT NULL,
  `episode` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `current_episodes`
--

INSERT INTO `current_episodes` (`id`, `episode`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-06-09 05:37:24', '2020-06-09 05:37:24');

-- --------------------------------------------------------

--
-- Estrutura da tabela `episodes`
--

CREATE TABLE IF NOT EXISTS `episodes` (
  `id` int(10) unsigned NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `episodes`
--

INSERT INTO `episodes` (`id`, `Title`, `Order`, `created_at`, `updated_at`) VALUES
(1, 'Juno', 1, '2020-06-09 05:25:01', '2020-06-09 05:25:01'),
(2, 'Amatsu e Kunlun', 2, '2020-06-09 05:25:10', '2020-06-09 05:25:10'),
(3, 'Umbala e Niflhein', 3, '2020-06-09 05:25:19', '2020-06-09 05:25:19'),
(4, 'Heritage', 4, '2020-06-09 05:25:25', '2020-06-09 05:25:25'),
(5, 'Einbroch', 5, '2020-06-09 05:25:32', '2020-06-09 05:25:32'),
(6, 'Lighthalzen & Noghaltz', 6, '2020-06-09 05:25:46', '2020-06-09 05:25:46'),
(7, 'Hugel', 7, '2020-06-09 05:25:54', '2020-06-09 05:25:54'),
(8, 'Rachel', 8, '2020-06-09 05:26:03', '2020-06-09 05:26:03'),
(9, 'Veins', 9, '2020-06-09 05:26:09', '2020-06-09 05:26:09'),
(10, 'Guerra do Emperium 2.0', 10, '2020-06-09 05:26:16', '2020-06-09 05:26:16'),
(11, 'A Ilha Esquecida', 11, '2020-06-09 05:26:22', '2020-06-09 05:26:22'),
(12, 'Moscóvia', 12, '2020-06-09 05:26:29', '2020-06-09 05:26:29'),
(13, 'A Ruína de Morroc', 13, '2020-06-09 05:26:36', '2020-06-09 05:26:36'),
(14, 'Ash Vacuum', 14, '2020-06-09 05:26:42', '2020-06-09 05:26:42'),
(15, 'Brasilis', 15, '2020-06-09 05:26:47', '2020-06-09 05:26:47'),
(16, 'Encontro com o Desconhecido', 16, '2020-06-09 05:26:53', '2020-06-09 05:26:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `reference` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `reference`, `created_at`, `updated_at`) VALUES
(1, 'Header', '<p>Ragna Memory</p>', '/header', '2020-06-09 05:44:22', '2020-06-09 05:44:22'),
(2, 'Footer', '<p>Todos os direitos reservados Ragna Memory™.</p>', '/footer', '2020-06-09 05:45:24', '2020-06-09 05:45:30'),
(3, 'Sobre o Ragna Memory', '<p>Teste</p>', '/about', '2020-06-09 05:46:34', '2020-06-09 05:46:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `quests`
--

CREATE TABLE IF NOT EXISTS `quests` (
  `id` int(10) unsigned NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Recipe` longtext NOT NULL,
  `city` int(11) DEFAULT NULL,
  `episode` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `quests`
--

INSERT INTO `quests` (`id`, `Title`, `Recipe`, `city`, `episode`, `created_at`, `updated_at`) VALUES
(1, 'Exemplo de Quest', '<p>&nbsp;</p><figure class="table"><table><tbody><tr><td colspan="2"><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Cabana Orc - Elmo Orc Herói - Passo 1</strong></span></p></td></tr><tr><td><p><span style="background-color:transparent;color:#00568a;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p><figure class="image"><img src="https://lh6.googleusercontent.com/OuR5PvA9-EXz_KmgyyOMRTN_qdu9F03mAYsgq3CcHt2yV4-lxahbi78nL7kFhV9KWJC_qRpKHQoJ77HbNR1TMUs4OBBaXKkjcKOWrv47q0uJ6gly5FCfEFeWEBHn2KeS8pEw4Jw" alt="http://file5.ratemyserver.net/quests/npcs/1023.gif"></figure><p style="text-align:center;"><span style="background-color:transparent;color:#ff6600;">Guerreiro Orc</span><br><span style="background-color:transparent;color:#00568a;">in_orcs01,31,93</span></p></td><td><p>&nbsp;</p><figure class="image"><img src="https://lh5.googleusercontent.com/U8pUVmP_rF0z4acu07nPBVgin8ULce6VTDgyrQ7B2CZVbsW6lgXX0SZ984sZHzwirYUZPumMLSaU0rOKKfWxByX-HnuiHSnpJ_tb9cNf4Nr7Gz5p1FZT6aR3AFJjm4UvmrJuw9k" alt="http://file5.ratemyserver.net/quests/minimaps/m10030_0.jpg"></figure><p style="margin-left:0pt;text-align:center;"><span style="background-color:transparent;color:#00568a;">Cabana na Vila dos Orcs</span></p></td></tr><tr><td><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Itens Necessários</strong></span></p></td><td><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Recompensa</strong></span></p></td></tr><tr><td><figure class="image image-style-align-left"><img src="https://lh6.googleusercontent.com/g1pNlMHC3iPrtxgrr1j23Yx4xF0xgqg3WZm1UcxEI-KkaTTMlZRjoRNqjHCTmJKCxMr83XSF97NFeTKTzEsoDQNswKd71i8UYyXuwZuI4lsQ9q4cR8tJ0wZTNRExcepCtBDxWl8" alt="http://file5.ratemyserver.net/items/small/909.gif"></figure><p style="margin-left:0pt;">&nbsp;</p><p style="margin-left:0pt;"><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=909"><span style="background-color:transparent;color:#00568a;"><u>Jellopy</u></span></a><span style="background-color:transparent;color:#00568a;">&nbsp;x 10000</span></p><p>&nbsp;</p><p style="margin-left:0pt;">&nbsp;</p></td><td><figure class="image image-style-align-left"><img src="https://lh5.googleusercontent.com/Hkw0NPFLdZ8z8_D7u6Ei1qXftezDZA2hU6PJ8Z_D9oRW6aTRvFITS2S7pg7P0diPA6OpDOrucMStwAtTfwrU4_NJZ5bgryI0N8nXyl6e_yekhdxLcCqiScGTs4RbCpsRabou5GQ" alt="http://file5.ratemyserver.net/items/small/1304.gif"></figure><p style="margin-left:0pt;">&nbsp;</p><p style="margin-left:0pt;"><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=1304"><span style="background-color:transparent;color:#00568a;"><u>Machado</u></span></a><span style="background-color:transparent;color:#00568a;"><u> Orc</u>&nbsp;x 1</span></p></td></tr><tr><td colspan="2"><span style="background-color:transparent;color:#00568a;">1° Fale com o Orc Guerreiro. Há 90% de chance dele pedir para você sair. Apenas fale com ele de novo e de novo até que ele concorde em conversar com você.&nbsp;</span><br><br><span style="background-color:transparent;color:#00568a;">2° Uma vez que você comece a conversar com ele, concorde em trazer-lhe 1000 Jellopys.</span><br><br><span style="background-color:transparent;color:#00568a;">3° Na verdade, basta coletar 10000 Jellopy.&nbsp;Ele pedirá 1000 Jellopy toda vez que você falar com ele, repita o processo 10 vezes,&nbsp; que soma 10000.&nbsp;</span><br><br><span style="background-color:transparent;color:#00568a;">4° Depois de dar 10000 Jellopys, ele lhe dará um&nbsp;<strong>Machado Orc</strong>&nbsp;como um agradecimento.&nbsp;</span><br><br><span style="background-color:transparent;color:#00568a;"><strong>Lembre-se de</strong>&nbsp;<strong>guardar o Machado Orc antes de falar com ele novamente</strong>&nbsp;(coloque-o em seu inventário, carrinho etc). Com o Machado Orc no seu inventário, ele não falará com você sobre o próximo passo.</span></td></tr></tbody></table></figure><p>&nbsp;</p><figure class="table"><table><tbody><tr><td colspan="2"><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Cabana Orc - Elmo Orc Herói - Passo 2</strong></span></p></td></tr><tr><td><p><span style="background-color:transparent;color:#00568a;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p><figure class="image"><img src="https://lh6.googleusercontent.com/OuR5PvA9-EXz_KmgyyOMRTN_qdu9F03mAYsgq3CcHt2yV4-lxahbi78nL7kFhV9KWJC_qRpKHQoJ77HbNR1TMUs4OBBaXKkjcKOWrv47q0uJ6gly5FCfEFeWEBHn2KeS8pEw4Jw" alt="http://file5.ratemyserver.net/quests/npcs/1023.gif"></figure><p style="text-align:center;"><span style="background-color:transparent;color:#ff6600;">Guerreiro Orc</span><br><span style="background-color:transparent;color:#00568a;">in_orcs01,31,93</span></p></td><td><p>&nbsp;</p><figure class="image"><img src="https://lh5.googleusercontent.com/U8pUVmP_rF0z4acu07nPBVgin8ULce6VTDgyrQ7B2CZVbsW6lgXX0SZ984sZHzwirYUZPumMLSaU0rOKKfWxByX-HnuiHSnpJ_tb9cNf4Nr7Gz5p1FZT6aR3AFJjm4UvmrJuw9k" alt="http://file5.ratemyserver.net/quests/minimaps/m10030_0.jpg"></figure><p style="margin-left:0pt;text-align:center;"><span style="background-color:transparent;color:#00568a;">Cabana na Vila dos Orcs</span></p></td></tr><tr><td><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Itens Necessários</strong></span></p></td><td><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Recompensa</strong></span></p></td></tr><tr><td><figure class="image image-style-align-left"><img src="https://lh4.googleusercontent.com/wuVA-m0Ti_liNWeAwMIUp0LxzqflDtBCFczoR3hneCPsALGuZ5P4qDSnfJhqsy0MWj20jr7RGthJK2vWzoRAuLKe-HfGeDMBWZ4_KEb3JrUciBIZSkrQAbAwVwaQhiGQd7b2KBU" alt="http://file5.ratemyserver.net/items/small/931.gif"></figure><p><span style="background-color:transparent;color:#00568a;">&nbsp;</span></p><p><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=931"><span style="background-color:transparent;color:#00568a;"><u>Amuleto</u></span></a><span style="background-color:transparent;color:#00568a;"><u> de Orc&nbsp;</u>x 10000</span></p><p style="margin-left:0pt;">&nbsp;</p></td><td><figure class="image image-style-align-left"><img src="https://lh4.googleusercontent.com/DAq5HMddT974W5JreyZ56bV9IulpTSHKstwxsS0YF7B0zuRaNVtxw-uoUVHvqId2aNRQ_-Cw7z-5aL-D70TiePc4MQ6AP4VABGEG8paclx41Eh3SSdrHeHqubVuNb8NntR--9w8" alt="http://file5.ratemyserver.net/items/small/2299.gif"></figure><p>&nbsp;</p><p><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=2299"><span style="background-color:transparent;color:#00568a;"><u>Elmo</u></span></a><span style="background-color:transparent;color:#00568a;"><u> de Orc</u>&nbsp;x 1</span></p><p>&nbsp;</p><figure class="image image-style-align-left"><img src="https://lh4.googleusercontent.com/wuVA-m0Ti_liNWeAwMIUp0LxzqflDtBCFczoR3hneCPsALGuZ5P4qDSnfJhqsy0MWj20jr7RGthJK2vWzoRAuLKe-HfGeDMBWZ4_KEb3JrUciBIZSkrQAbAwVwaQhiGQd7b2KBU" alt="http://file5.ratemyserver.net/items/small/931.gif"></figure><p>&nbsp;</p><p><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=931"><span style="background-color:transparent;color:#00568a;"><u>Amuleto</u></span></a><span style="background-color:transparent;color:#00568a;"><u> de Orc&nbsp;</u>x 1&nbsp;</span></p></td></tr><tr><td colspan="2"><span style="color:#00568a;">1°<strong> Sem o Machado Orc no seu inventário e com pelo menos 100 </strong></span><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=931"><span style="color:#00568a;"><u>Amuleto</u></span></a><span style="color:#00568a;"> de Orc<strong>,</strong>&nbsp;fale com ele novamente.&nbsp;Igual o ultimo passo, há 90% de chance de que ele repita a mesma coisa. Apenas continue falando com ele até que ele lhe peça os 10.000 Amutelo de Orc.&nbsp;</span><br><br><span style="color:#00568a;">2° Neste desafio, você pode trazer qualquer quantidade de Orcish Voucher para ele de tempos em tempos e ele irá gravá-lo.&nbsp;Quando o total atingir 10.000, ele lhe dará um </span><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=2299"><span style="color:#00568a;"><u>Elmo</u></span></a><span style="color:#00568a;"> de Orc e um </span><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=931"><span style="color:#00568a;"><u>Amuleto</u></span></a><span style="color:#00568a;"> de Orc como presente.&nbsp;<strong>Mantenha o </strong></span><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=2299"><span style="color:#00568a;"><strong><u>Elmo</u></strong></span></a><span style="color:#00568a;"><strong> de Orc e o </strong></span><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=931"><span style="color:#00568a;"><strong><u>Amuleto</u></strong></span></a><span style="color:#00568a;"><strong> de Orc, você precisará dele no final da missão.</strong></span></td></tr></tbody></table></figure><p>&nbsp;</p><figure class="table"><table><tbody><tr><td colspan="2"><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Cabana Orc - Elmo Orc Herói - Passo 3</strong></span></p></td></tr><tr><td><p><span style="background-color:transparent;color:#00568a;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p><figure class="image"><img src="https://lh6.googleusercontent.com/OuR5PvA9-EXz_KmgyyOMRTN_qdu9F03mAYsgq3CcHt2yV4-lxahbi78nL7kFhV9KWJC_qRpKHQoJ77HbNR1TMUs4OBBaXKkjcKOWrv47q0uJ6gly5FCfEFeWEBHn2KeS8pEw4Jw" alt="http://file5.ratemyserver.net/quests/npcs/1023.gif"></figure><p style="text-align:center;"><span style="background-color:transparent;color:#ff6600;">Guerreiro Orc</span><br><span style="background-color:transparent;color:#00568a;">in_orcs01,31,93</span></p></td><td><p>&nbsp;</p><figure class="image"><img src="https://lh5.googleusercontent.com/U8pUVmP_rF0z4acu07nPBVgin8ULce6VTDgyrQ7B2CZVbsW6lgXX0SZ984sZHzwirYUZPumMLSaU0rOKKfWxByX-HnuiHSnpJ_tb9cNf4Nr7Gz5p1FZT6aR3AFJjm4UvmrJuw9k" alt="http://file5.ratemyserver.net/quests/minimaps/m10030_0.jpg"></figure><p style="margin-left:0pt;text-align:center;"><span style="background-color:transparent;color:#00568a;">Cabana na Vila dos Orcs</span></p></td></tr><tr><td><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Itens Necessários</strong></span></p></td><td><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Recompensa</strong></span></p></td></tr><tr><td><figure class="image image-style-align-left"><img src="https://lh3.googleusercontent.com/_22dah0XG-eBug7RvVn7JeH-8lPKEJEgi6MuwULN8VrlrbkPZNzDr5J4ROD_T-c1hsAZ_HHQ0SwpiudA-713uWxYhhmoPiL8n1p501lm1SMbXjt1-LpZdHGb_8BoF-9YhK8yJ_c" alt="http://file5.ratemyserver.net/items/small/968.gif"></figure><p><span style="background-color:transparent;color:#00568a;">&nbsp;</span></p><p><span style="background-color:transparent;color:#00568a;"><u>Insígnia do Herói</u>&nbsp;x 100</span></p><p style="margin-left:0pt;">&nbsp;</p></td><td><figure class="image image-style-align-left"><img src="https://lh5.googleusercontent.com/vavPXJ5mOhYke_zDLYvLEU4Wvd0UE5g_JPSYrAiCyVp7IMeMdziX2VM61RuylMNi7Ue9j7xkT1pK73ViIFdZcG4JR3pbriAm15ylj9j-M1tiGGdnFFmu_bpE9jg-64wfSUOTLPk" alt="http://file5.ratemyserver.net/items/small/1124.gif"></figure><p>&nbsp;</p><p><span style="background-color:transparent;color:#00568a;">&nbsp;</span><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=1124"><span style="background-color:transparent;color:#00568a;"><u>Espada</u></span></a><span style="background-color:transparent;color:#00568a;"><u> Orc</u>&nbsp;x 1</span></p><p>&nbsp;</p><figure class="image image-style-align-left"><img src="https://lh3.googleusercontent.com/_22dah0XG-eBug7RvVn7JeH-8lPKEJEgi6MuwULN8VrlrbkPZNzDr5J4ROD_T-c1hsAZ_HHQ0SwpiudA-713uWxYhhmoPiL8n1p501lm1SMbXjt1-LpZdHGb_8BoF-9YhK8yJ_c" alt="http://file5.ratemyserver.net/items/small/968.gif"></figure><p>&nbsp;</p><p><span style="background-color:transparent;color:#00568a;"><u>Insígnia do Herói</u>&nbsp; x 1</span></p></td></tr><tr><td colspan="2"><span style="color:#00568a;">1°<strong> Com o Machado Orc no seu inventário,</strong>&nbsp;fale com ele novamente.&nbsp;O mesmo que da última vez, ele repetirá a mesma coisa de novo e de novo.&nbsp;Apenas continue falando com ele até que ele lhe ofereça o próximo teste.&nbsp;O qual é coletar 100 Insígnia do Herói&nbsp;.&nbsp;</span><br><br><span style="color:#00568a;">2° Uma vez que ele tenha suas 100 Insígnias do Herói&nbsp;, ele lhe dará uma Espada Orc e uma Insígnia do Herói&nbsp;.&nbsp;<strong>Mantenha a Espada Orc e a Insígnia do Herói</strong>&nbsp;<strong>, você precisará deles no final da missão.</strong></span></td></tr></tbody></table></figure><p>&nbsp;</p><figure class="table"><table><tbody><tr><td colspan="2"><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Cabana Orc - Elmo Orc Herói - Passo 4</strong></span></p></td></tr><tr><td><p><span style="background-color:transparent;color:#00568a;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p><figure class="image"><img src="https://lh4.googleusercontent.com/6kjrMQEOZbJ756M1KgTRHqbhYDqRFNNn0-a9huF1Xt1XsGkjq0WhK0I7rG0XMbjAQ48U4MFadOZYlntEzHtZq5ED4w4rbht2Ul9XyY7QzQPWPFwaL7aAbpN-sWnuSoEiGTrAZgQ" alt="http://file5.ratemyserver.net/quests/npcs/1087.gif"></figure><p style="text-align:center;"><span style="background-color:transparent;color:#ff6600;">Orc Herói</span><br><span style="background-color:transparent;color:#00568a;">in_orcs01,162,33</span></p></td><td><p>&nbsp;</p><p>&nbsp;</p><figure class="image"><img src="https://lh5.googleusercontent.com/U8pUVmP_rF0z4acu07nPBVgin8ULce6VTDgyrQ7B2CZVbsW6lgXX0SZ984sZHzwirYUZPumMLSaU0rOKKfWxByX-HnuiHSnpJ_tb9cNf4Nr7Gz5p1FZT6aR3AFJjm4UvmrJuw9k" alt="http://file5.ratemyserver.net/quests/minimaps/m10030_0.jpg"></figure><p style="margin-left:0pt;text-align:center;"><span style="background-color:transparent;color:#00568a;">Cabana na Vila dos Orcs</span></p></td></tr><tr><td><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Itens Necessários</strong></span></p></td><td><p style="text-align:center;"><span style="background-color:transparent;color:#00568a;"><strong>Recompensa</strong></span></p></td></tr><tr><td><figure class="image image-style-align-left"><img src="https://lh3.googleusercontent.com/_22dah0XG-eBug7RvVn7JeH-8lPKEJEgi6MuwULN8VrlrbkPZNzDr5J4ROD_T-c1hsAZ_HHQ0SwpiudA-713uWxYhhmoPiL8n1p501lm1SMbXjt1-LpZdHGb_8BoF-9YhK8yJ_c" alt="http://file5.ratemyserver.net/items/small/968.gif"></figure><p><span style="background-color:transparent;color:#00568a;">&nbsp;</span></p><p><span style="background-color:transparent;color:#00568a;"><u>Insígnia do Herói</u>&nbsp;x 1</span></p><figure class="image image-style-align-left"><img src="https://lh4.googleusercontent.com/DAq5HMddT974W5JreyZ56bV9IulpTSHKstwxsS0YF7B0zuRaNVtxw-uoUVHvqId2aNRQ_-Cw7z-5aL-D70TiePc4MQ6AP4VABGEG8paclx41Eh3SSdrHeHqubVuNb8NntR--9w8" alt="http://file5.ratemyserver.net/items/small/2299.gif"></figure><p>&nbsp;</p><p>&nbsp;</p><p><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=2299"><span style="background-color:transparent;color:#00568a;"><u>Elmo</u></span></a><span style="background-color:transparent;color:#00568a;"><u> Orc</u>&nbsp;x 1</span></p><figure class="image image-style-align-left"><img src="https://lh5.googleusercontent.com/vavPXJ5mOhYke_zDLYvLEU4Wvd0UE5g_JPSYrAiCyVp7IMeMdziX2VM61RuylMNi7Ue9j7xkT1pK73ViIFdZcG4JR3pbriAm15ylj9j-M1tiGGdnFFmu_bpE9jg-64wfSUOTLPk" alt="http://file5.ratemyserver.net/items/small/1124.gif"></figure><p>&nbsp;</p><p>&nbsp;</p><p><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=1124"><span style="background-color:transparent;color:#00568a;"><u>Espada</u></span></a><span style="background-color:transparent;color:#00568a;"><u> Orc</u>&nbsp;x 1&nbsp;</span></p><figure class="image image-style-align-left"><img src="https://lh4.googleusercontent.com/wuVA-m0Ti_liNWeAwMIUp0LxzqflDtBCFczoR3hneCPsALGuZ5P4qDSnfJhqsy0MWj20jr7RGthJK2vWzoRAuLKe-HfGeDMBWZ4_KEb3JrUciBIZSkrQAbAwVwaQhiGQd7b2KBU" alt="http://file5.ratemyserver.net/items/small/931.gif"></figure><p>&nbsp;</p><p><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=931"><span style="background-color:transparent;color:#00568a;"><u>Amuleto</u></span></a><span style="background-color:transparent;color:#00568a;"><u> de Orc</u>&nbsp;x 1</span></p><p>&nbsp;</p></td><td><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><figure class="image image-style-align-left"><img src="https://lh4.googleusercontent.com/Bddey1wUPUU8ymf-5qNIZLt9Aapt-4kGGFMLDlgPb512SLmaPYSCB1zMOIgoYf1Cpth9Mg6lHo7wof7FP-6uyK7dSGfzUJYDSzUCHoWTfEQb5lM0cwevoXwTCFwD4o7fbqkZNUk" alt="http://file5.ratemyserver.net/items/small/5094.gif"></figure><p>&nbsp;</p><p><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=5094"><span style="background-color:transparent;color:#00568a;"><u>Elmo</u></span></a><span style="background-color:transparent;color:#00568a;"><u> do Orc Herói</u></span></p></td></tr><tr><td colspan="2"><span style="color:#00568a;">1°<strong> Com o Elmo Orc, Amuleto de Orc, Espada Orc e <u>Insígnia do Herói</u></strong>&nbsp;<strong> no seu inventário,</strong>&nbsp;fale com o NPC Orc Herói.&nbsp;Semelhante ao NPC Guerreiro Orc, você tem que continuar falando com ele.&nbsp;Na verdade, há apenas 5% de chance de ele falar com você sobre o Elmo Orc.&nbsp;Apenas continue tentando.&nbsp;</span><br><br><span style="color:#00568a;">2° Se ele começar a falar sobre o Elmo dos Orcs, ele pedirá que você devolva o Elmo Orc que você tem na mão.&nbsp;Apenas dê a ele e ele o recompensará com o Elmo do Orc Herói.&nbsp;</span><br><br><span style="color:#00568a;">3° Você pode ficar com o resto dos itens, eles são necessários apenas para você mostrar ao NPC.&nbsp;Além disso, observe que você precisa concluir as etapas anteriores.&nbsp;Você não pode Pular as etapas da quest e simplesmente levar os itens para este NPC Orc Herói e esperar que ele lhe dê o </span><a href="http://ratemyserver.net/index.php?page=item_db&amp;item_id=5094"><span style="color:#00568a;"><strong><u>Elmo</u></strong></span></a><span style="color:#00568a;"><strong><u> do Orc Herói</u>.</strong></span></td></tr></tbody></table></figure>', NULL, NULL, '2020-06-09 06:08:02', '2020-06-09 06:08:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `strapi_administrator`
--

CREATE TABLE IF NOT EXISTS `strapi_administrator` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `resetPasswordToken` varchar(255) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `strapi_administrator`
--

INSERT INTO `strapi_administrator` (`id`, `username`, `email`, `password`, `resetPasswordToken`, `blocked`) VALUES
(1, 'Thor', 'davi.abreu.w@gmail.com', '$2a$10$aRww026aKJ4.kfNZdGL5/uvxCLFNmvKYtlGXn3FueJO5cd96mgzlG', NULL, NULL),
(2, 'ragnamemory', 'memoryragna@gmail.com', '$2a$10$WChlrdFK97jpjl0irvrf8uqyzFQdWdmFQ91tFlk/e0qB9d6xak5qS', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `strapi_webhooks`
--

CREATE TABLE IF NOT EXISTS `strapi_webhooks` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` longtext,
  `headers` longtext,
  `events` longtext,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `upload_file`
--

CREATE TABLE IF NOT EXISTS `upload_file` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `alternativeText` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `formats` longtext,
  `hash` varchar(255) NOT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `mime` varchar(255) NOT NULL,
  `size` decimal(10,2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `previewUrl` varchar(255) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `provider_metadata` longtext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `upload_file`
--

INSERT INTO `upload_file` (`id`, `name`, `alternativeText`, `caption`, `width`, `height`, `formats`, `hash`, `ext`, `mime`, `size`, `url`, `previewUrl`, `provider`, `provider_metadata`, `created_at`, `updated_at`) VALUES
(1, 'brasao_ragmemory_2', '', '', 600, 450, '{"thumbnail":{"hash":"thumbnail_brasao_ragmemory_2_3b30240451","ext":".png","mime":"image/png","width":208,"height":156,"size":50.26,"url":"https://res.cloudinary.com/dtiyvfrnn/image/upload/v1591670786/thumbnail_brasao_ragmemory_2_3b30240451.png","provider_metadata":{"public_id":"thumbnail_brasao_ragmemory_2_3b30240451","resource_type":"image"}},"small":{"hash":"small_brasao_ragmemory_2_3b30240451","ext":".png","mime":"image/png","width":500,"height":375,"size":239.25,"url":"https://res.cloudinary.com/dtiyvfrnn/image/upload/v1591670787/small_brasao_ragmemory_2_3b30240451.png","provider_metadata":{"public_id":"small_brasao_ragmemory_2_3b30240451","resource_type":"image"}}}', 'brasao_ragmemory_2_3b30240451', '.png', 'image/png', '282.21', 'https://res.cloudinary.com/dtiyvfrnn/image/upload/v1591670784/brasao_ragmemory_2_3b30240451.png', NULL, 'cloudinary', '{"public_id":"brasao_ragmemory_2_3b30240451","resource_type":"image"}', '2020-06-09 05:43:55', '2020-06-09 05:43:55'),
(2, 'edit9_trim', '', '', 2500, 1271, '{"thumbnail":{"hash":"thumbnail_edit9_trim_020e28fbed","ext":".jpeg","mime":"image/jpeg","width":245,"height":125,"size":7.56,"url":"https://res.cloudinary.com/dtiyvfrnn/image/upload/v1591670924/thumbnail_edit9_trim_020e28fbed.jpg","provider_metadata":{"public_id":"thumbnail_edit9_trim_020e28fbed","resource_type":"image"}},"large":{"hash":"large_edit9_trim_020e28fbed","ext":".jpeg","mime":"image/jpeg","width":1000,"height":508,"size":67.71,"url":"https://res.cloudinary.com/dtiyvfrnn/image/upload/v1591670925/large_edit9_trim_020e28fbed.jpg","provider_metadata":{"public_id":"large_edit9_trim_020e28fbed","resource_type":"image"}},"medium":{"hash":"medium_edit9_trim_020e28fbed","ext":".jpeg","mime":"image/jpeg","width":750,"height":381,"size":43.89,"url":"https://res.cloudinary.com/dtiyvfrnn/image/upload/v1591670926/medium_edit9_trim_020e28fbed.jpg","provider_metadata":{"public_id":"medium_edit9_trim_020e28fbed","resource_type":"image"}},"small":{"hash":"small_edit9_trim_020e28fbed","ext":".jpeg","mime":"image/jpeg","width":500,"height":254,"size":23.38,"url":"https://res.cloudinary.com/dtiyvfrnn/image/upload/v1591670927/small_edit9_trim_020e28fbed.jpg","provider_metadata":{"public_id":"small_edit9_trim_020e28fbed","resource_type":"image"}}}', 'edit9_trim_020e28fbed', '.jpeg', 'image/jpeg', '253.48', 'https://res.cloudinary.com/dtiyvfrnn/image/upload/v1591670923/edit9_trim_020e28fbed.jpg', NULL, 'cloudinary', '{"public_id":"edit9_trim_020e28fbed","resource_type":"image"}', '2020-06-09 05:46:15', '2020-06-09 05:46:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `upload_file_morph`
--

CREATE TABLE IF NOT EXISTS `upload_file_morph` (
  `id` int(10) unsigned NOT NULL,
  `upload_file_id` int(11) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  `related_type` longtext,
  `field` longtext,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `upload_file_morph`
--

INSERT INTO `upload_file_morph` (`id`, `upload_file_id`, `related_id`, `related_type`, `field`, `order`) VALUES
(1, 1, 1, 'pages', 'logo', 1),
(3, 2, 3, 'pages', 'logo', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users-permissions_permission`
--

CREATE TABLE IF NOT EXISTS `users-permissions_permission` (
  `id` int(10) unsigned NOT NULL,
  `type` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `policy` varchar(255) DEFAULT NULL,
  `role` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users-permissions_permission`
--

INSERT INTO `users-permissions_permission` (`id`, `type`, `controller`, `action`, `enabled`, `policy`, `role`) VALUES
(1, 'application', 'page', 'find', 0, '', 1),
(2, 'application', 'page', 'findone', 0, '', 1),
(3, 'application', 'page', 'count', 0, '', 1),
(4, 'application', 'page', 'create', 0, '', 1),
(5, 'application', 'page', 'update', 0, '', 1),
(6, 'content-manager', 'components', 'findcomponent', 0, '', 1),
(7, 'content-manager', 'components', 'listcomponents', 0, '', 1),
(8, 'content-manager', 'components', 'updatecomponent', 0, '', 1),
(9, 'application', 'page', 'delete', 0, '', 1),
(10, 'content-manager', 'contentmanager', 'generateuid', 0, '', 1),
(11, 'content-manager', 'contentmanager', 'checkuidavailability', 0, '', 1),
(12, 'content-manager', 'contentmanager', 'find', 0, '', 1),
(13, 'content-manager', 'contentmanager', 'findone', 0, '', 1),
(14, 'content-manager', 'contentmanager', 'count', 0, '', 1),
(15, 'content-manager', 'contentmanager', 'create', 0, '', 1),
(16, 'content-manager', 'contentmanager', 'update', 0, '', 1),
(17, 'content-manager', 'contentmanager', 'delete', 0, '', 1),
(18, 'content-manager', 'contenttypes', 'listcontenttypes', 0, '', 1),
(19, 'content-manager', 'contenttypes', 'findcontenttype', 0, '', 1),
(20, 'content-manager', 'contentmanager', 'deletemany', 0, '', 1),
(21, 'content-manager', 'contenttypes', 'updatecontenttype', 0, '', 1),
(22, 'content-type-builder', 'componentcategories', 'deletecategory', 0, '', 1),
(23, 'content-type-builder', 'componentcategories', 'editcategory', 0, '', 1),
(24, 'content-type-builder', 'components', 'getcomponents', 0, '', 1),
(25, 'content-type-builder', 'components', 'createcomponent', 0, '', 1),
(26, 'content-type-builder', 'components', 'updatecomponent', 0, '', 1),
(27, 'content-type-builder', 'components', 'getcomponent', 0, '', 1),
(28, 'content-type-builder', 'components', 'deletecomponent', 0, '', 1),
(29, 'content-type-builder', 'connections', 'getconnections', 0, '', 1),
(30, 'content-type-builder', 'contenttypes', 'getcontenttypes', 0, '', 1),
(31, 'content-type-builder', 'contenttypes', 'getcontenttype', 0, '', 1),
(32, 'content-type-builder', 'contenttypes', 'createcontenttype', 0, '', 1),
(33, 'content-type-builder', 'contenttypes', 'updatecontenttype', 0, '', 1),
(34, 'content-type-builder', 'contenttypes', 'deletecontenttype', 0, '', 1),
(35, 'email', 'email', 'send', 0, '', 1),
(36, 'email', 'email', 'getenvironments', 0, '', 1),
(37, 'email', 'email', 'getsettings', 0, '', 1),
(38, 'email', 'email', 'updatesettings', 0, '', 1),
(39, 'upload', 'proxy', 'uploadproxy', 0, '', 1),
(40, 'upload', 'upload', 'upload', 0, '', 1),
(41, 'upload', 'upload', 'getsettings', 0, '', 1),
(42, 'upload', 'upload', 'updatesettings', 0, '', 1),
(43, 'upload', 'upload', 'find', 0, '', 1),
(44, 'upload', 'upload', 'findone', 0, '', 1),
(45, 'upload', 'upload', 'count', 0, '', 1),
(46, 'upload', 'upload', 'destroy', 0, '', 1),
(47, 'upload', 'upload', 'search', 0, '', 1),
(48, 'users-permissions', 'auth', 'callback', 0, '', 1),
(49, 'users-permissions', 'auth', 'changepassword', 0, '', 1),
(50, 'users-permissions', 'auth', 'connect', 1, '', 1),
(51, 'users-permissions', 'auth', 'forgotpassword', 0, '', 1),
(52, 'users-permissions', 'auth', 'register', 0, '', 1),
(53, 'users-permissions', 'auth', 'emailconfirmation', 0, '', 1),
(54, 'users-permissions', 'auth', 'sendemailconfirmation', 0, '', 1),
(55, 'users-permissions', 'user', 'find', 0, '', 1),
(56, 'users-permissions', 'user', 'findone', 0, '', 1),
(57, 'users-permissions', 'user', 'me', 1, '', 1),
(58, 'users-permissions', 'user', 'create', 0, '', 1),
(59, 'users-permissions', 'user', 'update', 0, '', 1),
(60, 'users-permissions', 'user', 'destroy', 0, '', 1),
(61, 'users-permissions', 'user', 'destroyall', 0, '', 1),
(62, 'users-permissions', 'userspermissions', 'createrole', 0, '', 1),
(63, 'users-permissions', 'userspermissions', 'deleteprovider', 0, '', 1),
(64, 'users-permissions', 'userspermissions', 'deleterole', 0, '', 1),
(65, 'users-permissions', 'userspermissions', 'getpermissions', 0, '', 1),
(66, 'users-permissions', 'userspermissions', 'getpolicies', 0, '', 1),
(67, 'users-permissions', 'userspermissions', 'getrole', 0, '', 1),
(68, 'users-permissions', 'userspermissions', 'getroutes', 0, '', 1),
(69, 'users-permissions', 'userspermissions', 'getroles', 0, '', 1),
(70, 'users-permissions', 'userspermissions', 'index', 0, '', 1),
(71, 'users-permissions', 'userspermissions', 'init', 1, '', 1),
(72, 'users-permissions', 'userspermissions', 'searchusers', 0, '', 1),
(73, 'users-permissions', 'userspermissions', 'updaterole', 0, '', 1),
(74, 'users-permissions', 'userspermissions', 'getemailtemplate', 0, '', 1),
(75, 'users-permissions', 'userspermissions', 'updateemailtemplate', 0, '', 1),
(76, 'users-permissions', 'userspermissions', 'updateadvancedsettings', 0, '', 1),
(77, 'users-permissions', 'userspermissions', 'getadvancedsettings', 0, '', 1),
(78, 'users-permissions', 'userspermissions', 'getproviders', 0, '', 1),
(79, 'users-permissions', 'userspermissions', 'updateproviders', 0, '', 1),
(80, 'application', 'page', 'find', 1, '', 2),
(81, 'application', 'page', 'count', 0, '', 2),
(82, 'application', 'page', 'findone', 1, '', 2),
(83, 'application', 'page', 'create', 0, '', 2),
(84, 'application', 'page', 'update', 0, '', 2),
(85, 'application', 'page', 'delete', 0, '', 2),
(86, 'content-manager', 'components', 'listcomponents', 0, '', 2),
(87, 'content-manager', 'components', 'findcomponent', 0, '', 2),
(88, 'content-manager', 'components', 'updatecomponent', 0, '', 2),
(89, 'content-manager', 'contentmanager', 'generateuid', 0, '', 2),
(90, 'content-manager', 'contentmanager', 'checkuidavailability', 0, '', 2),
(91, 'content-manager', 'contentmanager', 'findone', 0, '', 2),
(92, 'content-manager', 'contentmanager', 'find', 0, '', 2),
(93, 'content-manager', 'contentmanager', 'count', 0, '', 2),
(94, 'content-manager', 'contentmanager', 'create', 0, '', 2),
(95, 'content-manager', 'contentmanager', 'update', 0, '', 2),
(96, 'content-manager', 'contentmanager', 'delete', 0, '', 2),
(97, 'content-manager', 'contentmanager', 'deletemany', 0, '', 2),
(98, 'content-manager', 'contenttypes', 'listcontenttypes', 0, '', 2),
(99, 'content-manager', 'contenttypes', 'findcontenttype', 0, '', 2),
(100, 'content-manager', 'contenttypes', 'updatecontenttype', 0, '', 2),
(101, 'content-type-builder', 'componentcategories', 'editcategory', 0, '', 2),
(102, 'content-type-builder', 'componentcategories', 'deletecategory', 0, '', 2),
(103, 'content-type-builder', 'components', 'getcomponents', 0, '', 2),
(104, 'content-type-builder', 'components', 'getcomponent', 0, '', 2),
(105, 'content-type-builder', 'components', 'createcomponent', 0, '', 2),
(106, 'content-type-builder', 'components', 'updatecomponent', 0, '', 2),
(107, 'content-type-builder', 'components', 'deletecomponent', 0, '', 2),
(108, 'content-type-builder', 'connections', 'getconnections', 0, '', 2),
(109, 'content-type-builder', 'contenttypes', 'getcontenttypes', 0, '', 2),
(110, 'content-type-builder', 'contenttypes', 'getcontenttype', 0, '', 2),
(111, 'content-type-builder', 'contenttypes', 'createcontenttype', 0, '', 2),
(112, 'content-type-builder', 'contenttypes', 'updatecontenttype', 0, '', 2),
(113, 'content-type-builder', 'contenttypes', 'deletecontenttype', 0, '', 2),
(114, 'email', 'email', 'send', 0, '', 2),
(115, 'email', 'email', 'getenvironments', 0, '', 2),
(116, 'email', 'email', 'getsettings', 0, '', 2),
(117, 'upload', 'upload', 'upload', 0, '', 2),
(118, 'email', 'email', 'updatesettings', 0, '', 2),
(119, 'upload', 'proxy', 'uploadproxy', 0, '', 2),
(120, 'upload', 'upload', 'getsettings', 0, '', 2),
(121, 'upload', 'upload', 'updatesettings', 0, '', 2),
(122, 'upload', 'upload', 'find', 0, '', 2),
(123, 'upload', 'upload', 'findone', 0, '', 2),
(124, 'upload', 'upload', 'count', 0, '', 2),
(125, 'upload', 'upload', 'destroy', 0, '', 2),
(126, 'upload', 'upload', 'search', 0, '', 2),
(127, 'users-permissions', 'auth', 'callback', 1, '', 2),
(128, 'users-permissions', 'auth', 'changepassword', 1, '', 2),
(129, 'users-permissions', 'auth', 'connect', 1, '', 2),
(130, 'users-permissions', 'auth', 'forgotpassword', 1, '', 2),
(131, 'users-permissions', 'auth', 'register', 1, '', 2),
(132, 'users-permissions', 'auth', 'emailconfirmation', 1, '', 2),
(133, 'users-permissions', 'auth', 'sendemailconfirmation', 0, '', 2),
(134, 'users-permissions', 'user', 'find', 0, '', 2),
(135, 'users-permissions', 'user', 'me', 1, '', 2),
(136, 'users-permissions', 'user', 'findone', 0, '', 2),
(137, 'users-permissions', 'user', 'create', 0, '', 2),
(138, 'users-permissions', 'user', 'update', 0, '', 2),
(139, 'users-permissions', 'user', 'destroy', 0, '', 2),
(140, 'users-permissions', 'user', 'destroyall', 0, '', 2),
(141, 'users-permissions', 'userspermissions', 'createrole', 0, '', 2),
(142, 'users-permissions', 'userspermissions', 'deleteprovider', 0, '', 2),
(143, 'users-permissions', 'userspermissions', 'deleterole', 0, '', 2),
(144, 'users-permissions', 'userspermissions', 'getpermissions', 0, '', 2),
(145, 'users-permissions', 'userspermissions', 'getroles', 0, '', 2),
(146, 'users-permissions', 'userspermissions', 'getrole', 0, '', 2),
(147, 'users-permissions', 'userspermissions', 'getroutes', 0, '', 2),
(148, 'users-permissions', 'userspermissions', 'getpolicies', 0, '', 2),
(149, 'users-permissions', 'userspermissions', 'index', 0, '', 2),
(150, 'users-permissions', 'userspermissions', 'init', 1, '', 2),
(151, 'users-permissions', 'userspermissions', 'searchusers', 0, '', 2),
(152, 'users-permissions', 'userspermissions', 'updaterole', 0, '', 2),
(153, 'users-permissions', 'userspermissions', 'getemailtemplate', 0, '', 2),
(154, 'users-permissions', 'userspermissions', 'updateemailtemplate', 0, '', 2),
(155, 'users-permissions', 'userspermissions', 'getadvancedsettings', 0, '', 2),
(156, 'users-permissions', 'userspermissions', 'updateadvancedsettings', 0, '', 2),
(157, 'users-permissions', 'userspermissions', 'getproviders', 0, '', 2),
(158, 'users-permissions', 'userspermissions', 'updateproviders', 0, '', 2),
(159, 'application', 'city', 'findone', 0, '', 1),
(160, 'application', 'city', 'find', 0, '', 1),
(161, 'application', 'city', 'count', 0, '', 1),
(162, 'application', 'city', 'create', 0, '', 1),
(163, 'application', 'city', 'update', 0, '', 1),
(164, 'application', 'current-episode', 'find', 0, '', 1),
(165, 'application', 'current-episode', 'update', 0, '', 1),
(166, 'application', 'episode', 'find', 0, '', 1),
(167, 'application', 'current-episode', 'delete', 0, '', 1),
(168, 'application', 'city', 'delete', 0, '', 1),
(169, 'application', 'episode', 'findone', 0, '', 1),
(170, 'application', 'episode', 'count', 0, '', 1),
(171, 'application', 'episode', 'create', 0, '', 1),
(172, 'application', 'episode', 'update', 0, '', 1),
(173, 'application', 'episode', 'delete', 0, '', 1),
(174, 'application', 'quest', 'find', 0, '', 1),
(175, 'application', 'quest', 'findone', 0, '', 1),
(176, 'application', 'quest', 'create', 0, '', 1),
(177, 'application', 'quest', 'count', 0, '', 1),
(178, 'application', 'quest', 'update', 0, '', 1),
(179, 'application', 'quest', 'delete', 0, '', 1),
(180, 'application', 'city', 'find', 1, '', 2),
(181, 'application', 'city', 'findone', 1, '', 2),
(182, 'application', 'city', 'count', 0, '', 2),
(183, 'application', 'city', 'create', 0, '', 2),
(184, 'application', 'city', 'update', 0, '', 2),
(185, 'application', 'current-episode', 'find', 1, '', 2),
(186, 'application', 'current-episode', 'update', 0, '', 2),
(187, 'application', 'city', 'delete', 0, '', 2),
(188, 'application', 'current-episode', 'delete', 0, '', 2),
(189, 'application', 'episode', 'find', 1, '', 2),
(190, 'application', 'episode', 'findone', 1, '', 2),
(191, 'application', 'episode', 'count', 0, '', 2),
(192, 'application', 'episode', 'create', 0, '', 2),
(193, 'application', 'episode', 'update', 0, '', 2),
(194, 'application', 'episode', 'delete', 0, '', 2),
(195, 'application', 'quest', 'find', 1, '', 2),
(196, 'application', 'quest', 'findone', 1, '', 2),
(197, 'application', 'quest', 'count', 0, '', 2),
(198, 'application', 'quest', 'create', 0, '', 2),
(199, 'application', 'quest', 'update', 0, '', 2),
(200, 'application', 'quest', 'delete', 0, '', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users-permissions_role`
--

CREATE TABLE IF NOT EXISTS `users-permissions_role` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users-permissions_role`
--

INSERT INTO `users-permissions_role` (`id`, `name`, `description`, `type`) VALUES
(1, 'Authenticated', 'Default role given to authenticated user.', 'authenticated'),
(2, 'Public', 'Default role given to unauthenticated user.', 'public');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users-permissions_user`
--

CREATE TABLE IF NOT EXISTS `users-permissions_user` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `resetPasswordToken` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cities_Name_unique` (`Name`),
  ADD FULLTEXT KEY `SEARCH_CITIES` (`Name`);

--
-- Indexes for table `core_store`
--
ALTER TABLE `core_store`
  ADD PRIMARY KEY (`id`),
  ADD FULLTEXT KEY `SEARCH_CORE_STORE` (`key`,`value`,`type`,`environment`,`tag`);

--
-- Indexes for table `current_episodes`
--
ALTER TABLE `current_episodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `episodes`
--
ALTER TABLE `episodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `episodes_Title_unique` (`Title`),
  ADD UNIQUE KEY `episodes_Order_unique` (`Order`),
  ADD FULLTEXT KEY `SEARCH_EPISODES` (`Title`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD FULLTEXT KEY `SEARCH_PAGES` (`title`,`reference`);

--
-- Indexes for table `quests`
--
ALTER TABLE `quests`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `quests_Title_unique` (`Title`),
  ADD FULLTEXT KEY `SEARCH_QUESTS` (`Title`);

--
-- Indexes for table `strapi_administrator`
--
ALTER TABLE `strapi_administrator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `strapi_administrator_username_unique` (`username`),
  ADD FULLTEXT KEY `SEARCH_STRAPI_ADMINISTRATOR` (`username`,`resetPasswordToken`);

--
-- Indexes for table `strapi_webhooks`
--
ALTER TABLE `strapi_webhooks`
  ADD PRIMARY KEY (`id`),
  ADD FULLTEXT KEY `SEARCH_STRAPI_WEBHOOKS` (`name`,`url`);

--
-- Indexes for table `upload_file`
--
ALTER TABLE `upload_file`
  ADD PRIMARY KEY (`id`),
  ADD FULLTEXT KEY `SEARCH_UPLOAD_FILE` (`name`,`alternativeText`,`caption`,`hash`,`ext`,`mime`,`url`,`previewUrl`,`provider`);

--
-- Indexes for table `upload_file_morph`
--
ALTER TABLE `upload_file_morph`
  ADD PRIMARY KEY (`id`),
  ADD FULLTEXT KEY `SEARCH_UPLOAD_FILE_MORPH` (`related_type`,`field`);

--
-- Indexes for table `users-permissions_permission`
--
ALTER TABLE `users-permissions_permission`
  ADD PRIMARY KEY (`id`),
  ADD FULLTEXT KEY `SEARCH_USERS_PERMISSIONS_PERMISSION` (`type`,`controller`,`action`,`policy`);

--
-- Indexes for table `users-permissions_role`
--
ALTER TABLE `users-permissions_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users-permissions_role_type_unique` (`type`),
  ADD FULLTEXT KEY `SEARCH_USERS_PERMISSIONS_ROLE` (`name`,`description`,`type`);

--
-- Indexes for table `users-permissions_user`
--
ALTER TABLE `users-permissions_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users-permissions_user_username_unique` (`username`),
  ADD FULLTEXT KEY `SEARCH_USERS_PERMISSIONS_USER` (`username`,`provider`,`resetPasswordToken`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `core_store`
--
ALTER TABLE `core_store`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `current_episodes`
--
ALTER TABLE `current_episodes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `episodes`
--
ALTER TABLE `episodes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `quests`
--
ALTER TABLE `quests`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `strapi_administrator`
--
ALTER TABLE `strapi_administrator`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `strapi_webhooks`
--
ALTER TABLE `strapi_webhooks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `upload_file`
--
ALTER TABLE `upload_file`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `upload_file_morph`
--
ALTER TABLE `upload_file_morph`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users-permissions_permission`
--
ALTER TABLE `users-permissions_permission`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=201;
--
-- AUTO_INCREMENT for table `users-permissions_role`
--
ALTER TABLE `users-permissions_role`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users-permissions_user`
--
ALTER TABLE `users-permissions_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
